require "sinatra"
require "twitter"

set :server, 'webrick'

config = {
  consumer_key: ENV['TWITTER_CONSUMER_KEY'],
  consumer_secret: ENV['TWITTER_CONSUMER_SECRET'],
}

client = Twitter::REST::Client.new(config)

get "/" do
  @recent_tweets = client.user_timeline(screen_name: "twitter")
  erb :home
end

get "/:username" do
  username = params[:username]
  @recent_tweets = client.user_timeline(screen_name: username)
  erb :home
end
